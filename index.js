import _ from 'lodash'
import speakingurl from 'speakingurl'
import markdown from 'markdown-it'
import markdownToc from 'markdown-it-toc'
import currencyFormatter from 'currency-formatter'
import moment from 'moment'
import loremIpsum from 'lorem-ipsum'
import striptags from 'striptags'

const md = markdown( { breaks: true, html: true } ).use( markdownToc )

export default locale => {
  return {
    _,
    slug: string => speakingurl( string ),
    md: string => string ? md.render( string ) : '',
    formatDate: ( date, format ) => moment( date ).format( format || 'MMMM Do YYYY' ),
    formatPrice: ( price, currency ) => currencyFormatter.format( price, { code: currency || locale.currency } ),
    loremIpsum: ( count = 1, units = 'paragraphs', format = 'plain' ) => loremIpsum( { count, units, format } ),
    striptags,

    facebookShareUrl: ( url, image ) => `https://www.facebook.com/sharer/sharer.php?u=${ encodeURIComponent( url ) }&picture=${ encodeURIComponent( image ) }`,
    pinterestShareUrl: ( url, image, description ) => `https://pinterest.com/pin/create/button/?url=${ encodeURIComponent( url ) }&media=${ encodeURIComponent( image ) }&description=${ encodeURIComponent( description ) }`,
    twitterShareUrl: ( url, heading ) => `https://www.twitter.com/home/?status=${ encodeURIComponent( heading ) }%20${ encodeURIComponent( url ) }`
  }
}
